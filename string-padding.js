/* 
 String padding
 by James Swineson (@Jamesits)
 https://gist.github.com/Jamesits/30acd725d5fdfb67eae4
 Make a string fixed length by adding blank chars between or after it.
 Usage: 
 	"123".lpadding(5)      // returns "  123"
 	"123".lpadding(5, "0") // returns "00123"
 	"123".lpadding(2)      // returns "23"
 	"123".rpadding(5)      // returns "123  "
 	"123".rpadding(5, "0") // returns "12300"
 	"123".rpadding(2)      // returns "12"
 CAUTION: no boundry check and arguments out-of-range check. Use at your own risk. 
 */
 
String.prototype.lpadding || (String.prototype.lpadding = function(len, char) {
    char = typeof char !== 'undefined' ? char : ' ';
    return (Array(len + 1).join(char) + this).slice(-len);
});

String.prototype.rpadding || (String.prototype.rpadding = function(len, char) {
    char = typeof char !== 'undefined' ? char : ' ';
    return (this + Array(len + 1).join(char)).slice(0, len);
});
